<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
	h2{text-align:center}
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
		.imp{font-size:200%}
		.commitee{margin-bottom:5%}
    </style>
<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="font-family: Ubuntu">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>
	
	<div class="container box-effect animated bounceInLeft">
		
			<div class="about-info" >
				
				<p></p>
			</div>
			<div class="about-grids" >
				<div class="col-md-6 about-left" style="border:1px solid #34495e;padding:3%;">
					<div class="about-pad" >
					  
						
								<h2>Chief Patron</h2>
								<h3 style="color:##00bcd4">Sri. A. S. N. Prasad </h3>
								<p align="justify" style="font-size:16px;margin-top:0%">B.E, F.I.E, Director Sri Rama Corporation<br>
								<h2>Patrons</h2>
								<h3 style="color:##00bcd4">Prof. Ing. P. Srinivasa Rao</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Advisor (R&D), GVP, Director General, GVP-SIRC<br>
								<h3 style="color:##00bcd4">Prof. P. Soma Raju</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Secretary,GVPCOE(A)</p>
								<h3 style="color:##00bcd4">Prof. Dr. A. B. Koteswara Rao</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Principal, GVPCOE(A), Visakhapatnam</p>

								<h2>Honorary General Chairs</h2>
								<h3 style="color:##00bcd4">Dr. Rajib Mall</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor , IIT, Kharagpur</p>
								<h3 style="color:##00bcd4">Dr. Suresh Chandra Satapathy</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor & Head, PVP Siddhartha Institute of Technology</p>
								
								
								
								<h2>Honorary Advisory Chair</h2>
								<h3 style="color:##00bcd4">Prof. P.K. Dash</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">S 'O'A University, India</p>
								
								
								<h2>General Advisory Board</h2>
								<h3 style="color:##00bcd4">Prof. B. V. Ramana Murthy</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean of Academic Programs-UG</p>
								<h3 style="color:##00bcd4">Prof. Y. V. P. K. Raghava</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean of Student Affairs</p>
								<h3 style="color:##00bcd4">Prof. P. Veerabhadra Rao </h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean of Infrastructure, Planning and Development</p>
								<h3 style="color:##00bcd4">Prof. C. V. K. Bhanu </h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean of Academic Programs-PG and Research Program </p>
								
								<h2>Program Chairs</h2>
								<h3 style="color:##00bcd4">Dr. Sukumar Mishra , FNAE,FNSc</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, IIT Delhi, India</p>
								<h3 style="color:##00bcd4">Dr. Birendra Biswal</h3> 
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, ECE, GVPCE (A),Visakhapatnam,India</p>
								
								
								<h2>Organizing Chairs</h2>
								<h3 style="color:##00bcd4">Mr. G. Anand Kumar</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dept. of ECE </p>
								<br>
								<br>
								<br>
								
								<h2>Organizing co-Chairs</h2>
								<h3 style="color:##00bcd4">Dr. M. V. S. Sairam</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, Head of the Department, ECE</p>						
								<h3 style="color:##00bcd4">Dr. N. Bala Subrahmanyam</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, ECE</p>
								<h3 style="color:##00bcd4">Dr. D. B. V. Jaganadham</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, ECE</p> 
								<h3 style="color:##00bcd4">Dr. N. Deepika Rani</h3> 
								<p align="justify" style="font-size:16px;margin-top:0%">Professor, ECE</p> 								
								
								

								
								<h2>Publicity Chairs</h2>
								<h3 style="color:##00bcd4">Dr. P. Venkata Rao</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean, Training & Placements</p>
								<h3 style="color:##00bcd4">Mr. P. Srinu</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dept. of ECE</p>
								
								
								<h2>Finance Chair</h2>
								<h3 style="color:##00bcd4">Prof. S. Atchutaramam </h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Dean, Administration</p>
								
								
								<h2>Sponsorship  Chair</h2>
								<h3 style="color:##00bcd4">Dr. B. Jagadeesh</h3>
								<p align="justify" style="font-size:16px;margin-top:0%">Associate Professor, Dept. of ECE</p>
								
								
								<h2>Website Chair</h2>
								<h3 style="color:##00bcd4">Sri G. Rajeswara Rao</h3>
								<p align="justify" style="font-size:16px;margin-top:0%"> Dept. of ECE</p>
								
								
								<h2>Registration Chairs</h2>
								<h3 style="color:##00bcd4">Dr. V. Leela Rani</h3> 
								<p align="justify" style="font-size:16px;margin-top:0%">Dept. of ECE</p>
								<h3 style="color:##00bcd4">Dr. Ch. Kusuma Kumari</h3> 
								<p align="justify" style="font-size:16px;margin-top:0%">Dept. of ECE</p>
								<h3 style="color:##00bcd4">Smt. A. Naga Malli</h3> 
								<p align="justify" style="font-size:16px;margin-top:0%">Dept. of ECE</p>
																
								
							  <h2>Technical Chairs</h2>
								<p align="justify" style="font-size:16px;margin-top:0%">
									<br>
									<h3>Dr. P. Rajesh Kumar</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, Andhra University</p>
									<h3>Dr. G. Sasibhushan Rao</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, Andhra University</p>
									<h3>Dr. P. V. Sridevi</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, Andhra University</p>
									<h3>Dr. P. Mallikarjuna Rao</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, Andhra University</p>
									<h3>Dr. B. Prabhakar Rao</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, JNTUK Kakinada</p>
									<h3>Dr. Srinivas Kumar</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, JNTUK Kakinada</p>
									<h3>Dr. Padma Raju</h3>
									<p align="justify" style="font-size:16px;margin-top:0%">Professor, JNTUK Kakinada</p>
									<h4>                    </h4>
									<h4>      </h4>
													</p>
									
									

									

															
									
									
								</p>
						
						
					</div>
					<div style="margin-bottom:8%"></div>
				</div>
				<div class="col-md-6 about-left" style="border:1px solid #34495e;padding:3%">
					<div class="about-pad" style="padding-left:5%;" >
					  
							<h2>International Technical Advisory Board</h2>
							<h3 style="color:##00bcd4">Prof. Saifur Rahman</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">Virginia Polytechnic Institute & State University, U.S.A</p>
							<h3 style="color:##00bcd4">Prof. Lalu Mansinha</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">University of Western Ontario, Canada</p>
							<h3 style="color:##00bcd4">Prof. Bernard Mulgrew</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">University of Edinburg, UK </p>
							<h3 style="color:##00bcd4">Prof. Dr. Goi Bok Min</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">Universiti Tunku Abdul Rahman (UTAR),Malaysia</p>
							<h3 style="color:##00bcd4">Prof. Adel M. Sharaf</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">University of New Brunswick, Canada</p>
							<h3 style="color:##00bcd4">Prof. A. C. Liew</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">National University of Singapore, Singapore</p>
							<h3 style="color:##00bcd4">Prof. Stella Morish</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">Multi Media University, Malaysia</p>
							<h3 style="color:##00bcd4">Prof. Dipti Srinivasan</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">NUS,Singapore</p>
							<h3 style="color:##00bcd4">Prof. Geza Joos</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">McGill School of Environment, Canada</p>
							<h3 style="color:##00bcd4">Prof. Loi Lei Lai</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">City University of London, London</p>
							<h3 style="color:##00bcd4">Prof. Z. Y. Dong</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">Hongkong Polytechnic University, Hongkong</p>
							<h3 style="color:##00bcd4">Prof. P. K. Meher</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">Nanyang Technological University, Singapore</p>
							<h3 style="color:##00bcd4">Prof. Sagar Naik</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">University of Waterloo, Canada<br>
							<h3 style="color:##00bcd4">Prof. R.K. Gokararaju</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">University of Saskatchewan, Canada<br>
							<h3 style="color:##00bcd4">Prof. Nick Jenkins</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">Cardiff School of Engineering, UK<br>
							<h3 style="color:##00bcd4">Prof. Rajiv. K. Varma</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">University of Western Ontario, Canada<br>
							<h3 style="color:##00bcd4">Prof. Fran Li</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">Ph.D., P.E., The University of Tennessee ,  U.S.A.<br>
							<h3 style="color:##00bcd4">Prof. Ramesh Bansal</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">University of Queensland, Australia<br>
							<h3 style="color:##00bcd4">Prof. Amit Mishra</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">University of  Capetown, South Africa<br>
							<h3 style="color:##00bcd4">Prof. A. K. Swain</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">University of Auckland, New Zealand </p>
							 
							 
							 
							 
							 
							<h2>National Technical Advisory Board</h2>
							<h3 style="color:##00bcd4">Prof. B. K. Panigrahi</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">IIT Delhi, India</p>
							<h3 style="color:##00bcd4">Prof. Sivaji Chakravorti</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">Jadavpur University, Kolkata, India </p>
							<h3 style="color:##00bcd4">Prof. Ganapati Panda</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">IIT Bhubaneswar, India</p>
							<h3 style="color:##00bcd4">Prof. H. P. Khincha</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">IISC, Bangalore, India</p>
							<h3 style="color:##00bcd4">Prof. Ashok Pradhan</h3>
							<p align="justify" style="font-size:16px;margin-top:0%">IIT Kharagpur, India</p>
							<h3 style="color:##00bcd4">Prof. Aurobinda Routray</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">IIT Kharagpur ,India</p>
							<h3 style="color:##00bcd4">Prof. S. R. Samantaray</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">I.I.T. Bhubaneswar, India</p>
							<h3 style="color:##00bcd4">Prof. S. C. Srivastava</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">IIT, Kanpur, India</p>
							<h3 style="color:##00bcd4">Prof. M. S. Manikandan</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">IIT,Bhubaneswar, India</p>
							<h3 style="color:##00bcd4">Prof. Dr. C. N. Bhende </h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">I.I.T. Bhubaneswar, India</p>
							<h3 style="color:##00bcd4">Prof. Dr. Pravas Ranjan Sahu</h3> 
							<p align="justify" style="font-size:16px;margin-top:0%">I.I.T. Bhubaneswar, India</p>
						
						
							
							
							
							
							<h2>Organizing Committee</h2>
							<p align="justify" style="font-size:16px;margin-top:0%;">	
						  	<h4>Sri. K. R. K. Sastry <br>
						 	<h4>Dr. Dr. B. S. Rao<br>
						 	<h4>Dr. Debasish Bera<br>
						 	<h4>Dr.	G. Padma<br>
						 	<h4>Smt. P. Aruna Kumari <br>
						 	<h4>Mr. Sagara Pandu <br>
						 	<h4>Mr. Sagar Krishna Sivvam <br>
						 	<h4>Mr. K. Naresh Kumar <br>
						 	<h4>Mr. K. Satya Krishna Murthy <br>
						 	<h4>Mr. N. Santosh Kumar<br>
						 	<h4>Mrs. M. Neelima<br>
						 	<h4>Mr. S. M. K. Chaitanya<br>
						 	<h4>Mrs. G. Radha Kumari<br>					 
						 	<h4>Mr. N. Venkatesh<br>
						 	<h4>Mr. Ch. Venkanna<br>
						 	<h4>Mr. Sreenivasulu Mamilla<br>
						 	<h4>Mrs. B. Keerthi Priya<br>
						 	<h4>Mr. Tammineni Ravindra<br>
						 	<h4>Mr. Dadi Sita Siva<br>
						 	<h4>Mrs. N. Santoshi<br>
						 	<h4>Mrs. P. Pavani<br>
						 	<h4>Dr. T. Vidhyavathi<br>
						 	<h4>Mr. B. R. M. Krishna<br>
						 	<h4>Miss. K. Jhansi Rani<br>
						 	<h4>Miss. U. Divya Sree<br>
						 	<h4>Mrs. A. M. C. H. Jyothi <br>
						 	<h4>Mrs. A. Raja Sree </h4>
						 	<h4>Mrs. P. R. Sri Satya</h4>
						 	<br>
						 
					</div>

				</div>
			</div>
		</div>
		

			
<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>
</body>
</html>