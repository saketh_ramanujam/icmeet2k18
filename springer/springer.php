<div class="col-md-4" style="font-family: Ubuntu;">

	<button type="button" class="btn 	btn-info"><a href="specialsessions.php">Special Sessions<sup><span class="blink_text">new</span></sup></a></button>
	<h6 class="font_6" style="font-size:29px; text-align:center;"><span style="color:#F00732;"><span style="font-family:times new roman,times,serif;"><span style="font-weight:bold;"><span style="font-size:29px;color:#F00732">Springer LNEE Series<br></span></span></span></span></h6>
	<div>
		<img class="img-responsive" src="./images/springer.png"/>
	</div>
	<ul>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="/docs/ICMEET-2018.pdf" target="_self" data-anchor="SCROLL_TO_BOTTOM"><span style="letter-spacing:0em;"><span class="color_20">Brochure</span></span></a></span></span></h6>
		</li>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="callforpaper.php" target="_self"><span style="letter-spacing:0em;"><span class="color_20">Call For Papers</span></span></a></span></span></h6>
		</li>

		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><a href="https://easychair.org/conferences/?conf=icmeet2k18" target="_self"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><span style="letter-spacing:0em;"><span class="color_20">Paper Submission</span></span></span></span></a></h6>
		</li>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="shortlist.php" target="_self" data-anchor="SCROLL_TO_BOTTOM"><span style="letter-spacing:0em;"><span class="color_20">Short Listed Papers&nbsp;</span></span></a></span></span></h6>
		</li>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><a href="guidelines.php" target="_self" data-type="document"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><span style="letter-spacing:0em;"><span class="color_20">Springer Copyright Form</span></span></span></span></a></h6>
		</li>

		<li>
		<h6 style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="impdates.php" target="_self"><span style="letter-spacing:0em;"><span class="color_20">Important Dates</span></span></a></span></span></h6>
		</li>
		<!--<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="underconstruction.php" target="_self" data-anchor="SCROLL_TO_BOTTOM"><span style="letter-spacing:0em;"><span class="color_20">Program at a Glance</span></span></a></span></span></h6>
		</li>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="underconstruction.php" target="_self" data-anchor="SCROLL_TO_BOTTOM"><span style="letter-spacing:0em;"><span class="color_20">Program Schedule</span></span></a></span></span></h6>
		</li>-->		
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="https://www.tripadvisor.in/SmartDeals-g297588-Visakhapatnam_Visakhapatnam_District_Andhra_Pradesh-Hotel-Deals.html" target="_self" data-anchor="SCROLL_TO_BOTTOM"><span style="letter-spacing:0em;"><span class="color_20">Accomodation</span></span></a></span></span></h6>
		</li>
		<li>
		<h6 class="font_6" style="font-size:24px; line-height:1.1em;"><span style="font-size:24px;"><span style="font-family:times new roman,times,serif;"><a href="contactus.php" target="_self"><span style="letter-spacing:0em;"><span class="color_20">Contact Us</span></span></a></span></span></h6>
		</li>
	</ul>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Icmeet -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3737050365676330"
     data-ad-slot="6095813022"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>


<style type="text/css">
	.blink_text {

    animation:1s blinker linear infinite;
    -webkit-animation:1s blinker linear infinite;
    -moz-animation:1s blinker linear infinite;

     color: red;
    }

    @-moz-keyframes blinker {  
     0% { opacity: 1.0; }
     50% { opacity: 0.0; }
     100% { opacity: 1.0; }
     }

    @-webkit-keyframes blinker {  
     0% { opacity: 1.0; }
     50% { opacity: 0.0; }
     100% { opacity: 1.0; }
     }

    @keyframes blinker {  
     0% { opacity: 1.0; }
     50% { opacity: 0.0; }
     100% { opacity: 1.0; }
     }
</style>