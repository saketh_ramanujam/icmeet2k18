﻿<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
    </style>
<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<style>
.tech_com{
	list-style-type:none;
	font-size:150%;
	color:#003566;
	margin: 0px 0px 3% 5%;
}
</style>
</head>
<body style="font-family: Ubuntu;">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>
	<div class="container text-center">
		<h3 style="color:##00daf6">We solicit papers describing significant new findings in the following or related areas:</h3>
	</div>
	<hr>
	<div class="container box-effect animated bounceInLeft">
		<div class="col-md-8 about-grids">
          <li class="tech_com">Analog, Digital and Mixed Signal Circuits</li>
          <li class="tech_com">Bio-Medical Circuits and Systems</li>
          <li class="tech_com">RF Circuit Design</li>
          <li class="tech_com">Microwave and Millimeter Wave Circuits</li>
          <li class="tech_com"> Green Circuits and Systems</li>
          <li class="tech_com">Analog and Digital Signal Processing</li>
          <li class="tech_com">Nano Electronics and Giga Scale Systems</li>
          <li class="tech_com">VLSI Circuits and Systems</li>
          <li class="tech_com">SoC and NoC</li>
          <li class="tech_com">MEMS and NEMS</li>
          <li class="tech_com"> VLSI Digital signal processing</li>
          <li class="tech_com">Wireless Communications</li>
          <li class="tech_com">Cognitive Radio</li>
          <li class="tech_com">Data Communications</li>
          <li class="tech_com">Audio &amp; Speech Processing</li>
          <li class="tech_com"> Optical Communications and Networks</li>
          <li class="tech_com">Computational Electromagnetics</li>
          <li class="tech_com">Transmission lines and waveguides</li>
          <li class="tech_com">Antenna Design</li>
          <li class="tech_com">GPS &amp; GPRS</li>
          <li class="tech_com"> Image Processing</li>
          <li class="tech_com">Soft Computing</li>
          <li class="tech_com">Metaheuristics Techniques</li>
          <li class="tech_com">Neural Networks</li>

         <!--<span class="text-center">
          <a href="downloads/calforpaper.pdf" target="new" style="margin-left:254px;font-size: 20px;">Download call for papers (Pdf)</a>
         </span>-->
          
        </div>
		
		<?php 
			$path=$_SERVER['DOCUMENT_ROOT'];
			$path.="/springer/springer.php";
			include_once($path);
		?>
	</div>
	
			
<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>
</body>
</html>