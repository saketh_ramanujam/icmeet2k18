<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
		
    </style>
	

<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="font-family: Ubuntu;">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>

<div class="container box-effect animated bounceInLeft" >
	<div class="col-md-8">
	<table class="table table-stripped table-bordered" >
		<caption style="color:#00535d;">Contact Details</caption>
		<tr class="custom-table">
			<td style="font-weight: bold;font-size:20px;">Name</td>
			<td style="font-weight: bold;font-size:20px;">Contact</td>
		</tr>
		<tr>
			<td style="font-weight: bold;font-size:20px;">Dr. Birendra Biswal<br>
                <sub>
            Program Chair <br>
            ICMEET-2K18<br>
            </sub>

            </td>
			<td style="font-weight: bold;font-size:15px;">
            icmeet2k18@gmail.com<br><a href="mailto:birendrabiswal@gvpce.ac.in">birendrabiswal@gvpce.ac.in</a><br>+ 91-891-2739507<br> (Extn.390,391)<br>
            Ph: +91 9000405565
            </td>
		</tr>
		<tr>
			<td style="font-weight: bold;font-size:20px;">Mr. G. Anand Kumar<br><sub>Organizing Chair <br></sub><br>
            

            </td>
			<td style="font-weight: bold;font-size:15px;">
                
			<a href="mailto:ganand@gvpce.ac.in">ganand@gvpce.ac.in</a><br>
            Ph: +91 9949478761
            </td>
		</tr>
        <tr>
            <!--
            <td style="font-weight: bold;font-size:20px;">
            R.Manoj Kumar</td>

            <td style="font-weight: bold;font-size:20px;"><a href="mailto:manojr@gvpce.ac.in">manojr@gvpce.ac.in</a><br>Ph: +91 8143-665-300
            -->
	</table>
    <table class="table table-stripped table-bordered" >
        <caption style="color:#00535d;">Developer Details</caption>
        <tr class="custom-table">
            <td style="font-weight: bold;font-size:20px;">Name</td>
            <td style="font-weight: bold;font-size:20px;">Contact<br></td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px;">Mr. P. Durga Rao<br>
            
            <td style="font-weight: bold;font-size:15px;"><p>
            <a href = "mailto:developericmeet@gmail.com">developericmeet@gmail.com</a><br>
            
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px;">Mr. P. Rakesh <br>
          
            </td>
            <td style="font-weight: bold;font-size:15px;"><a href = "mailto:developericmeet@gmail.com">developericmeet@gmail.com</a><br>
       
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px;">Mr. S. S. S. Ramanujam <br>
           
            </td>
            <td style="font-weight: bold;font-size:15px;"><a href = "mailto:developericmeet@gmail.com">developericmeet@gmail.com</a>

           
            </td>
        </tr>
        </table>
        <br>
                <br>
                <br>
                <br>
                <br>
               
	<!--<center><input type="button" class="btn btn-primary btn-large" style="font-size:150%" value="Click Here for Contact Form" data-toggle="modal" data-target="#contactForm"></center>
	<div id="contactForm" class="modal fade" role="dialog">
     <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h2 class="modal-title" style="color:#FF69B4;text-align:center">Contact Form</h2>
      </div>
      <div class="modal-body">
        <form role="form" action="email.php" method="post">
		  <div class="form-group">
			<input type="text" class="form-control" name="name" placeholder="Name" required>
		  </div>
		  <div class="form-group">
			<input type="email" class="form-control" name="email" placeholder="Email" required>
		  </div>
		  <div class="form-group">
			<input type="text" class="form-control" name="phone" placeholder="Mobile Number" required>
		  </div>
		  <div class="form-group">
		  <textarea class="form-control" rows="5" name="comment" placeholder="Message" required></textarea>
		</div>
		  <center><button type="submit" class="btn btn-default" style="color:#FF69B4;text-align:center;font-size:120%">Submit</button></center>
		</form>-->
      </div>
      
    </div>

  </div>
</div>
</div>
		<?php 
			$path=$_SERVER['DOCUMENT_ROOT'];
			$path.="/springer/springer.php";
			include_once($path);
		?>
</div>



<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>

</body>
</html>