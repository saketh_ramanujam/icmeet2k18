<!DOCTYPE html>
<html>
	<head>
		<title>ICMEET 2K18</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
		<link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,600' rel='stylesheet' type='text/css'>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-3737050365676330",
		    enable_page_level_ads: true
		  });
		</script>
		
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/animate.css">
		<link rel="stylesheet" type="text/css" href="engine1/style.css" />
		<script type="text/javascript" src="engine1/jquery.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<style>
			body {font: normal 0.9em Arial;margin:0;}
			a {color:#1155CC;}
			ul li {padding: 10px 0;}
			header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
			header a {
				font-family: sans-serif;
				font-size: 24px;
				line-height: 24px;
				padding: 8px 13px 7px;
				color: #fff;
				text-decoration:none;
				transition: color 0.7s;
			}
			header a.active {
				font-weight:bold;
				width: 24px;
				height: 24px;
				padding: 4px;
				text-align: center;
				display:inline-block;
				border-radius: 50%;
				background: #C00;
				color: #fff;
			}
			.image {
				background-image: url("images/1.jpg");
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-position: center;
				background-size: cover;
				margin-top:-1.5%;
				min-height: 900px;
			}
		</style>
		<link rel="icon" href="favicon.jpg">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body style="font-family: Ubuntu;">
		<?php 
		$path=$_SERVER['DOCUMENT_ROOT'];
		$path.="/navigation/nav.php";
		include_once($path);
		?>
		<!--<div style="align-content: right">
			<a href="http://www.reliablecounter.com" target="_blank"><img src="http://www.reliablecounter.com/count.php?page=icmeet2k18.in&digit=style/plain/1/&reloads=0" alt="" title="" border="0"></a><br /><a href="http://" target="_blank" style="font-family: Geneva, Arial; font-size: 9px; color: #330010; text-decoration: none;"></a>

			<div data-type="countdown" data-id="206064" class="tickcounter" style="width: 100%;height:25%; position: relative; padding-bottom: 25%"><a href="//www.tickcounter.com/countdown/206064/icmeet-2k18" title="ICMEET 2k18">ICMEET 2k18</a><a href="//www.tickcounter.com/" title="Countdown">Countdown</a></div><script>(function(d, s, id) { var js, pjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//www.tickcounter.com/static/js/loader.js"; pjs.parentNode.insertBefore(js, pjs); }(document, "script", "tickcounter-sdk"));</script>-->

		</div>
		<div class="container animated bounceInLeft" style="margin-top:0%;z-index:2;position:relative;text-align: justify;text-justify: inter-word;">
			<div class="col-md-8">
				<p class="para">
					
						International Conference on Microelectronics, Electromagnetics and Telecommunication (ICMEET) is a highly respected forum, 
						which will provide the platform to attract academicians, scientists, research scholars and practitioners from premier institutes, 
						Universities, Engineering Colleges and R&D institutes from all over the world to share the updated breakthroughs and 
						promising solutions of the most important issues faced by the society. 
						
				</p>
				<p class="para">
					
						The forum will facilitate to discuss the most recent developments and future trends in the fields of Microelectronics, 
						Electromagnetics and Telecommunication. Research in Microelectronics explores semiconductor materials and device physics 
						for developing electronic devices and integrated circuits with both data and energy efficient performance in terms of speed, 
						power consumption and functionality.However, electromagnetics research engrosses the creation, transmission, and recognition 
						of electromagnetic energy. Current research work in these areas include development of fast algorithms and higher-order 
						numerical methods in computational electromagnetic along with development of numerical techniques for multi-scale and 
						multi-physics modeling that can be used to study electromagnetic phenomena and improve system performance.
					 
				</p>
				<p class="para">
					 
						Reconfigurable antennas can be used in multifunctional, cognitive, sensing systems and remote sensing of the atmosphere to analyze and
						predict performance problems with GPS and other communication systems. There is a huge requirement for innovations in the field of 
						telecommunication. Technology for effective utilization of frequency resources, high speed data communications, 
						power saving of wireless communication devices, GPS-based relative positioning method is the need of the hour.
					
				</p>
				<p class="para">
					
						This is an opportunity for researchers and practitioners across the world to exchange their ideas and 
						latest research findings on the above said areas. ICMEET -18 will be a premier international conference for its high quality 
						research in the above said areas. On this ground, GVP College of Engineering (A), Madhurawada is organizing an International 
						conference ICMEET -18 during 4th -5th Jan, 2018.
					
				</p>
				<p class="para">
					<b>
						Selected Papers will be published in Springer Journal: "Lecture Notes in Electrical Engineering".
					</b>
				</p>
				<!--<button class="btn btn-large btn-primary"><a href="about.php">Know More</a></button>-->
			</div>
			<?php 
				$path=$_SERVER['DOCUMENT_ROOT'];
				$path.="/springer/springer.php";
				include_once($path);
				?>
			<hr/>
		</div>
		<div class="container">
			<div class="footer text-center"><p class="op" style="margin-top:2%;margin-bottom:-1%">&copy; <?php echo date("Y") ?> All Rights Reserved</p> <br> <p style="margin-bottom:3%;">Gayatri Vidya Parishad College of Engineering (A) - E.C.E. Department</p></div>
		</div>
	</body>
</html>
<!--
462066	FFB85F	FF7A5A	00AAA0	8ED2C9	FCF4D9
-->