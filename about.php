<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
    </style>
<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="font-family: Ubuntu;">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>

	<div class="container box-effect animated bounceInLeft">
		
			<div class="about-info" >
				
				<p></p>
			</div>
			<div class="about-grids col-md-8" >
				<div >
					<div class="about-pad">
					  <h3>About GVPCE(A), Visakhapatnam</h3>
					  <a href="http://www.gvpce.ac.in" target="_blank"><b>www.gvpce.ac.in</b></a>
					  <br>

					  <p align="justify">
						Gayatri Vidya Parishad (GVP) has been established in the year 1988 as an educational trust by a group of eminent educationists, academicians & industrialists to empower the young generation through high quality technical education. The Engineering education by GVP society was first originated by establishing an Engineering College in the year 1996 with the divine blessings of Sadguru Sri. K. Sivananda Murthy Garu. 


						The institute has flourished in various facets of research & academics by achieving the pinnacle of success in the state of divided Andhra Pradesh. GVP College of Engineering (A) has been accredited by NAAC with "A" grade with CGPA of 3.47/4.00 in the year 2009 and subsequently reaccredited second time in the year 2016 with the same CGPA on the same scale. All it &apos:s under graduate programs are re-accredited by NBA. The institute was granted autonomous status by UGC and was conferred by the affiliating university JNTUK, Kakinada in the year 2009.The status of autonomy is further extended by UGC up to 2020. In addition to the above milestones, the institute has been bestowed with "A" grade industrial accreditation by TATA consultancy services and "A" grade ranking by Govt. of AP knowledge mission also. 


						In fact the institute has bagged another mammoth and prestigious landmark by receiving funds to the tune of 4 Crore under sub component 1.2 of technical education quality improvement program phase II (TEQIP-II) in the country. The institute encourages collaborative learning between industry and academia as a means of reinforcing its curriculum with practical and real world experiences.
						<br>
						<br>
					  </p>
						<h3>About The Department</h3>
						<p align="justify">
							Department of Electronics and Communication Engineering at GVP College of Engineering, Visakhapatnam was established in the year 2000.The Department offers UG Programme with an intake of 240 and two PG Programs in Communications Engineering & Signal Processing and VLSI Design & Embedded Systems with an intake of 18 . UG Programme is reaccredited by National Board of Accreditation. Both UG and PG Programmes are approved by AICTE and are affiliated to Jawaharlal Nehru Technological University (JNTUK), Kakinada. The department was recognized as research centre in the year 2014.
							The department is functioning with 40 faculty members among whom, 6 are Ph. D holders and 10 are pursuing PhDs and remaining are M.Tech holders from NITs and reputed universities. All the PhD holders have research scholars to pursue research in their respective areas.
							Faculty attend workshops, conferences and faculty development programmes being conducted at IITs, NITs and Universities and published papers in reputed national and international journals. The department has completed Projects worth 40 Lakhs sanctioned by AICTE and R & D organizations.
							The Faculty members have made a mark in the area of Innovative Hardware Design, Modelling & Analysis and developing new techniques and Algorithms in the fields of Communication Systems and Wireless Network, Signal and Image Processing and VLSI Design.
							The department has adequate hardware to support academic and research related activities. Analytical and simulation tools like Cadence, Xilinx, LABView, Multisim, MATlab, CCStudio, ARM cortex M3 are available to carry R&D Activities.
							The department is associated with professional bodies IEEE and IETE through student branches. Association of Electronics and Communication Engineering (AECE) is a departmental student forum which aims to interact with the technical environs. This forum is an initiative of students and the department fully supports all the initiatives for successful achievements by AECE. To impart the needed technical knowledge, students are encouraged to get hands on experience through conduct of Hardware- Exhibition frequently and are motivated to present innovative ideas and attend Symposiums organized by reputed institutes like IITs, NITs and research organizations. Our students are working in Engineering Services, R&D organizations like ISRO, DRDO and Google, Lucent –Alcatel, Motrola, Intel, TEXAS Instruments, IBM, BOSCH, Analog Devices, Tata Elxi, Samsung, have taken up Post Graduate Programs in reputed Institutions in IISc, IITs, NITs, various Foreign Universities. Some of the Alumni have ventured into Entrepreneurial area and successfully running the businesses.
						</p>
						<h3>About The Conference</h3>
					  <p align="justify">
						International Conference on Microelectronics, Electromagnetics and Telecommunication (ICMEET) is a highly respected forum, which will provide the platform to attract academicians, scientists, research scholars and practitioners from premier institutes, Universities, Engineering Colleges and R&D institutes from all over the world to share the updated breakthroughs and promising solutions of the most important issues faced by the society.

						The forum will facilitate to discuss the most recent developments and future trends in the fields of Microelectronics, Electromagnetics and Telecommunication. Research in Microelectronics explores semiconductor materials and device physics for developing electronic devices and integrated circuits with both data and energy efficient performance in terms of speed, power consumption and functionality.However, electromagnetics research engrosses the creation, transmission and recognition of electromagnetic energy. Current research work in these areas include development of fast algorithms and higher-order numerical methods in computational electromagnetic along with development of numerical techniques for multi-scale and multi-physics modeling that can be used to study electromagnetic phenomena and improve system performance.

						Reconfigurable antennas can be used in multifunctional, cognitive, sensing systems and remote sensing of the atmosphere to analyze and predict performance problems with GPS and other communication systems. There is a huge requirement for innovations in the field of telecommunication. Technology for effective utilization of frequency resources, high speed data communications, power saving of wireless communication devices, GPS-based relative positioning method is the need of the hour.

						This is an opportunity for researchers and practitioners across the world to exchange their ideas and latest research findings on the above said areas. ICMEET -18 will be a premier international conference for its high quality research in the above said areas. On this ground, GVP College of Engineering (A), Madhurawada is organizing an International conference ICMEET -18 during 4th -5th Jan, 2018.
					</p>
						
					</div>
				</div>
				
			</div>
			<?php 
			$path=$_SERVER['DOCUMENT_ROOT'];
			$path.="/springer/springer.php";
			include_once($path);
			?>
	
			
			</div>
			
<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>
</body>
</html>