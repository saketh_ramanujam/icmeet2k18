<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
	h2{text-align:center}
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
		.imp{font-size:200%}
		.commitee{margin-bottom:5%}
    </style>
<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="font-family: Ubuntu;">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>
	<div class="container box-effect animated bounceInLeft">
		<div class="col-md-8">
		<p style="text-align:justify;font-size:120%">
           Authors are invited to submit manuscripts written in English. All contributions must be original, should not have been published elsewhere and should not be intended to be published elsewhere during the review period.
		</p>
		<p style="text-align:justify;font-size:120%">
            Papers must be prepared according to Springer's templates (MS Word or LaTeX format) for the series.3 (Maximum Pages 8. Rs. 1500/$50 for each additional page) 
		<br><br>
		 LNEE Template :<a href="downloads/LNEEFormat.pdf" target="_new">Download</a>
              <br><br>
              Springer Copyright Form :<a href="downloads/SpringerCopyrightForm.pdf" target="_new">Download</a>
              <br><br>
              Papers should be submitted through Easy Chair only through the following link;
              <br>
              <a href="https://easychair.org/conferences/?conf=icmeet2k18"><p><font size="+2" face="Roboto">ICMEET 2K18</font></p></a>
			  </p>
		</div>
		<?php 
			$path=$_SERVER['DOCUMENT_ROOT'];
			$path.="/springer/springer.php";
			include_once($path);
		?>
	</div>
			
<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>
</body>
</html>