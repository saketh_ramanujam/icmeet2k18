<?php
session_start();

?>
   <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <div class="container-fluid animated fadeInDown" id="div" style="width:100%;background-color:white;z-index:1020;">
        <a href="index.php" >
            <img src="images/banner.png" style="padding-left: 10%;" class="img img-responsive">
              <div style="text-align:center">
		      <p class="banner"><nobr>ICMEET - 2K18</nobr></p><br>
              <br>
              <p class="bannerdown"><nobr style="font-size: 70%">4<sup>th</sup> - 5<sup>th</sup> January </nobr></p><br>
              <br>
              <p class="caption" style="font-weight: bold;font-size: 20px;">International Conference on Micro-Electronics, Electromagnetics and Telecommunications
		  </div>
		</a>
    </div>  
    <div class="container-fluid animated jackInTheBox" style="height: 50px; z-index: -1px;">
            <nav class="navbar customnav " style="background-color:#4fc7fd;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
              
            </div>
            <div class="container-fluid" style="margin-left: 120px;margin-right: 10px">
            <div class="collapse navbar-collapse" id="navbar-collapse-1" >
                
                <style>
                        .button {
                        background-color:#0392d2;
                        font-size:20px;
                        font-family: Ubuntu;
                        width: 100%;
                        height:100%;
                        color: blue;
                        padding: 15px 32px;
                        text-align: center;
                        text-decoration: none;
                        display: inline-block;
                        font-size: 16px;
                        margin: 4px 2px;
                        cursor: pointer;
                        border-radius: 25px;
                        }
                </style>
                <ul class="nav navbar-nav">
                    <li><a class='button' href="index.php" >Home</a></button></li>
					<li><a class='button' href="about.php">About</a></button></li>
					<li><a class='button' href="guidelines.php">Guidelines</a></button></li>                  
                    <li><a class='button' href="callforpaper.php">Call for Paper</a></button></li>
                    <li><a class='button' href="shortlist.php">ShortListed Papers</a></button></li>
					<li><a class='button' href="registration.php">Registration</a></li>
                    <li><a class='button' href="impdates.php">Important Dates</a></button></li> 
                    <li><a class='button' href="committee.php">Committee</a></button></li>
                	<li><a class='button' href="contactus.php">Contact Us</a></button></li>
                    
                    
					
                </ul>
            </div>
            </div>
            <hr>            
			<div><marquee onmouseover="stop()" onmouseout="start()" style="color:red"><blink>Paper Submission Deadline is </blink><span><b>15-11-2017<br><span></marquee></div>
        </nav>
      <div>