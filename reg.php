<?php
  require('conn.php');
  if( isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['salutation']) )
  {
    $first_name = mysqli_real_escape_string($conn,$_POST['first_name']);
    $last_name = mysqli_real_escape_string($conn,$_POST['last_name']);
    $salutation = mysqli_real_escape_string($conn,$_POST['salutation']);
    $dob = mysqli_real_escape_string($conn,$_POST['dob']);
    $gender = mysqli_real_escape_string($conn,$_POST['gender']);
    $email = mysqli_real_escape_string($_POST['email']);
    $mob = mysqli_real_escape_string($conn,$_POST['mob']);
    $password = mysqli_real_escape_string($conn,md5($_POST['[password]']));
    $country = mysqli_real_escape_string($conn,$_POST['country']);
    $zip = mysqli_real_escape_string($conn,$_POST['zip']);
    $accom = mysqli_real_escape_string($conn,$_POST['accom_options']);
    
    $vc = md5('ic'.rand(0,100));
    $vc2 = md5('gvpce'.rand(100,150));

    $explo =explode('@',$email);

    $query = "INSERT INTO `users` (first_name,last_name,gender,dob,salutaiton,email,code,code2,password,mobile,zip,accom,v) VALUES ('$first_name','$last_name','$gender','$dob','$sal','$email','$vc','$vc2','$pwd1','$mob','$zip','$accom','0')";

    $res = mysqli_query($conn, $query);

    $email_len = strlen($email);
    $cons_email =substr($email,0,3).str_repeat('*',$email_len-strlen($explo[1])-3);

    include('reg-success.html');

    if($res) {
      sendVerificationMail($explo[0],$vc,$vc2,$email);
      $msg = 'Thank you for registering. Your account has been created, please verify it by clicking the activation link that has been sent to your email '.$cons_email.$explo[1];
      echo $msg;
    }
    else {
      $msg = "User registration failed <br/>" . mysqli_error($res);
      echo $msg;
    }
  }

  function sendVerificationMail($user,$vc,$vc2,$email){
    $to = $email;
    $sub = 'Signup | Verification';

    $message = '
      Thanks for signing up for <b> RAMSA 2017 </b> <br/>
      Please click the link below to activate your account <br/>
      http://localhost/ramsa/verify.php?user='.$email.'&hash1='.$vc.'&hash2='.$vc2.'
    ';
    $headers = 'MIME-Version: 1.0'."\r\n".'Content-type: text/html; charset=iso-8859-1'."\r\n".'From: Email Verification <no-reply@gvpce-ramsa17.in>'."\r\n".'X-Mailer: PHP/'.phpversion() ;

    mail($to, $sub, $message, $headers);
  }
?>