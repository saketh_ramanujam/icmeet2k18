<!DOCTYPE html>
<html>
<head>
<title>ICMEET 2K18</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script src="2/ninja-slider.js" type="text/javascript"></script>
    <style>
        body {font: normal 0.9em Arial;margin:0;}
        a {color:#1155CC;}
        ul li {padding: 10px 0;}
        header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
        header a {
            font-family: sans-serif;
            font-size: 24px;
            line-height: 24px;
            padding: 8px 13px 7px;
            color: #fff;
            text-decoration:none;
            transition: color 0.7s;
        }
        header a.active {
            font-weight:bold;
            width: 24px;
            height: 24px;
            padding: 4px;
            text-align: center;
            display:inline-block;
            border-radius: 50%;
            background: #C00;
            color: #fff;
        }
    </style>
<link rel="icon" href="favicon.jpg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body style="font-family: Ubuntu;">
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/navigation/nav.php";
include_once($path);
?>
<div class ="container animated bounceInLeft " style="border-radius:10px">
<div  class="box-effect ">
<h2 style="text-align:center;color:#00535d">IMPORTANT DATES</h2>
<p style="margin: 0 auto 45px auto;
    font-size: 14px;
    line-height:2.4em;
    color: #5f5d5d;
    width: 75%;
    text-align: center;
    font-weight:bold;">
To ensure publication of a paper in the Proceedings, at least one author has to register by submitting a normal registration fee within deadline as indicated below.<br></p>
<table  class="table table-bordered table-stripped" style="border:5;height:50%; width:80%; padding:50px; text-align:center;">
	<tr style="color:black;" >
		<td style="font-weight: bold;font-size:20px;">Paper Submission</td>
		<td style="font-weight: bold;font-size:20px;">15-11-2017</td></tr>
	<tr  style="color:black;">
		<td style="font-weight: bold;font-size:20px;">Notification of acceptance</td>
		<td style="font-weight: bold;font-size:20px;">30-11-2017</td></tr>
	<tr  style="color:black;">
		<td style="font-weight: bold;font-size:20px;">Camera Ready Paper Submission Deadline	</td>
		<td style="font-weight: bold;font-size:20px;">10-12-2017</td>
	</tr>
	<tr  style="color:black;">
		<td style="font-weight: bold;font-size:20px;">Author’s registration</td>
		<td style="font-weight: bold;font-size:20px;">15-12-2017</td>
	</tr>
	<tr  style="color:black;">
		<td style="font-weight: bold;font-size:20px;">Conference Dates</td>
		<td style="font-weight: bold;font-size:20px;">4<sup>th</sup> to 5<sup>th</sup> JAN, 2018</td>
	</tr>
</table>
</div>
</div>
<!--<div class="container box-effect text-center">
	<h2>Will be updated soon...</h2>
</div>-->
<br><br><br><hr>
<?php 
$path=$_SERVER['DOCUMENT_ROOT'];
$path.="/footer/footer.php";
include_once($path);
?>
</body>
</html>