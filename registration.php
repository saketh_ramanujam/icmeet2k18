<!DOCTYPE html>
<html>
	<head>
		<title>ICMEET 2K18</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
		<link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/animate.css">
		<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,600' rel='stylesheet' type='text/css'>
		
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="engine1/style.css" />
		<script type="text/javascript" src="engine1/jquery.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<style>
			body {font: normal 0.9em Arial;margin:0;}
			a {color:#1155CC;}
			ul li {padding: 10px 0;}
			header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
			header a {
				font-family: sans-serif;
				font-size: 24px;
				line-height: 24px;
				padding: 8px 13px 7px;
				color: #fff;
				text-decoration:none;
				transition: color 0.7s;
			}
			header a.active {
				font-weight:bold;
				width: 24px;
				height: 24px;
				padding: 4px;
				text-align: center;
				display:inline-block;
				border-radius: 50%;
				background: #C00;
				color: #fff;
			}
			.image {
				background-image: url("images/1.jpg");
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-position: center;
				background-size: cover;
				margin-top:-1.5%;
				min-height: 900px;
			}
		</style>
		<link rel="icon" href="favicon.jpg">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body style="font-family: Ubuntu;">
		<?php 
		$path=$_SERVER['DOCUMENT_ROOT'];
		$path.="/navigation/nav.php";
		include_once($path);
		?>
		<div class="container box-effect animated bounceInLeft">
			<div class="col-md-8">
				<p class="para" style="font-style: bold;text-align: center;transform-color:Blue; ">
					

						
				<h3  style="font-color:Blue">Author Registration</h3>
				​
				Every accepted article should be registered by at least one author to appear in the proceedings. The acceptance list of articles will be announced in website and the authors will be intimated regarding the acceptance through e-mail.
				​
				<h4 style="font-weight: bold">Preparing and uploading of the Camera Ready Article:</h4>
				      The final camera-ready copy of the papers must be in Springer LNEE format with a length of
				      maximum 10 pages. Over-length charges will apply for each extra page beyond 08 pages.
				      However, it is desired that maximum number of pages in an article should not exceed 10
				      pages in any circumstance. Check Author Guidelines for formatting the camera ready article.
				      Upload camera-ready PDF on to the Easychair, and submit source file(s) as .zip or .rar file. 
				<h4 style="font-weight: bold">Paying Registration Fee:</h4>
				      Calculate the registration fee according to your page length and type of authorship as directed
				      in REGISTRATION page and make payment using Foreign Telegraphic Transfer or Direct
				      Transfer or NEFT Transfer or Wire transfer (preferable) or demand draft. 
				<h4 style="font-weight: bold">Filling up of the Registration Form</h4> which can be found in MS WORD format (Provided in guidelines for authors
				<h4 style="font-weight: bold">Mailing the following items in .zip / .rar format to <h5 style="font-weight: bold;">icmeet2k18@easychair.org</h5></h4>
				        <ol>
				        <li> The Camera ready Article source files, preferably in MS Word format</li>
				        <li> Scanned copy of filled in Registration Form</li>
				        <li> Scanned copy of filled in Consent to Publish Form</li>
				        <li> Proof of studentship (valid on 05-DEC-2017) for availing Student Registration (if
				          applicable) signed by head of the department/course.</li>
				        <li> Scan of registration fee pay-in-slip/voucher of online transfer payable to ICMEET 2018
				           (whichever applicable)</li>
				           
				           </ol>
				           </p>
				<h3>Registration details</h3>
				
			<table class="table table-stripped table-bordered" >
					<tr class="custom-table">
					<td style="font-weight: bold;font-size:20px;">Registration Type</td>
					<td style="font-weight: bold;font-size:20px;">On/Before <br>15<sup>th</sup> Dec, 2017</td>
					<td style="font-weight: bold;font-size:20px;">After<br> 15<sup>th</sup> Dec, 2017</td>
					
					
				</tr>
				<tr>					
					<td style="font-weight: bold;font-size:20px;">Scientists from Industry/R&D Organization<br></td>
					<td style="font-weight: bold;font-size:20px;">INR.8000</td>
					<td style="font-weight: bold;font-size:20px;">INR.9000</td>
				</tr>
				<tr>
					<td style="font-weight: bold;font-size:20px;">Faculty members from educational institutes<br></td>
					<td style="font-weight: bold;font-size:20px;">INR.5000</td>
					<td style="font-weight: bold;font-size:20px;">INR.6000</td>
				</tr>
				
				<tr>					
					<td style="font-weight: bold;font-size:20px;">Research Scholars / PG Students<br></td>
					<td style="font-weight: bold;font-size:20px;">INR.2000</td>
					<td style="font-weight: bold;font-size:20px;">INR.3000</td>
				</tr>
				<tr>					
					<td style="font-weight: bold;font-size:20px;">Foriegn Delegates<br></td>
					<td style="font-weight: bold;font-size:20px;">USD 150$</td>
					<td style="font-weight: bold;font-size:20px;">USD 200$</td>
				</tr>
				</table>
				<p>
		        Extra page charges over 8 pages in Rs 1500 per page.
				Multiple papers: Authors having more than one paper, the second paper ( only one paper allowed) is to be regd with Rs 5000/- only for any category. The First paper will have usual regd fees.
				<h3>Account details:</h3>
			        <h4 style="font-weight: bold;">Account Name:</h4> Principal GVP College Of Engineering
			        <h4 style="font-weight: bold;">Account No:</h4> Updated Soon
			        <h4 style="font-weight: bold;">Type of account:</h4> Savings
			        <h4 style="font-weight: bold;">Bank and Branch Name</h4> State Bank of India,M.V.P Colony
			        <h4 style="font-weight: bold;">IFSC Code:</h4> SBIN0004158
			        </p>
			
    
									
				</p>
				
				<button class="btn btn-large btn-primary"><a href="https://easychair.org/conferences/?conf=icmeet2k18">Register Now</a></button>
				<br>
				<p>
					<b>
						Selected & Registered Papers will be published in Springer Journal: "Lecture Notes in Electrical Engineering".
					</b>
				</p>
			</div>
			<?php 
				$path=$_SERVER['DOCUMENT_ROOT'];
				$path.="/springer/springer.php";
				include_once($path);
				?>
			<hr/>
		</div>
		<div class="container">
			<div class="footer text-center"><p class="op" style="margin-top:2%;margin-bottom:-1%">&copy; <?php echo date("Y") ?> All Rights Reserved</p> <br> <p style="margin-bottom:3%;">Gayatri Vidya Parishad College of Engineering (A) - E.C.E. Department</p></div>
		</div>
	</body>
</html>
<!--
462066	FFB85F	FF7A5A	00AAA0	8ED2C9	FCF4D9
-->