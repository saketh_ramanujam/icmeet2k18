<!DOCTYPE html>
<html>
	<head>
		<title>ICMEET 2K18</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" type="text/css" href="css/animate.css">
		<link href='http://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
		<link href="2/ninja-slider.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,600' rel='stylesheet' type='text/css'>
		
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="engine1/style.css" />
		<script type="text/javascript" src="engine1/jquery.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<style>
			body {font: normal 0.9em Arial;margin:0;}
			a {color:#1155CC;}
			ul li {padding: 10px 0;}
			header {display:block;padding:60px 0 20px;text-align:center;position:absolute;top:8%;left:8%;z-index:4;}
			header a {
				font-family: sans-serif;
				font-size: 24px;
				line-height: 24px;
				padding: 8px 13px 7px;
				color: #fff;
				text-decoration:none;
				transition: color 0.7s;
			}
			header a.active {
				font-weight:bold;
				width: 24px;
				height: 24px;
				padding: 4px;
				text-align: center;
				display:inline-block;
				border-radius: 50%;
				background: #C00;
				color: #fff;
			}
			.image {
				background-image: url("images/1.jpg");
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-position: center;
				background-size: cover;
				margin-top:-1.5%;
				min-height: 900px;
			}
		</style>
		<link rel="icon" href="favicon.jpg">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body style="font-family: Ubuntu;">
		<?php 
		$path=$_SERVER['DOCUMENT_ROOT'];
		$path.="/navigation/nav.php";
		include_once($path);
		?>
		<div class="container box-effect animated bounceInLeft">
			<div class="col-md-8">
				<p class="para" style="font-style: bold;text-align: center;transform-color:Blue;font-size:30px; ">
					

						Yet To Be ShortListed
					
				</p>
				
					<b>
						Selected & Registered Papers will be published in Springer Journal: "Lecture Notes in Electrical Engineering".
					</b>
				</p>
				<!--<button class="btn btn-large btn-primary"><a href="about.php">Know More</a></button>-->
			</div>
			<?php 
				$path=$_SERVER['DOCUMENT_ROOT'];
				$path.="/springer/springer.php";
				include_once($path);
				?>
			<hr/>
		</div>
		<div class="container">
			<div class="footer text-center"><p class="op" style="margin-top:2%;margin-bottom:-1%">&copy; <?php echo date("Y") ?> All Rights Reserved</p> <br> <p style="margin-bottom:3%;">Gayatri Vidya Parishad College of Engineering (A) - E.C.E. Department</p></div>
		</div>
	</body>
</html>
<!--
462066	FFB85F	FF7A5A	00AAA0	8ED2C9	FCF4D9
-->